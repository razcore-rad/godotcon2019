extends Node


onready var player: Area2D = $Player
onready var mob_spawner: PathFollow2D = $MobPath/MobSpawner
onready var hud: CanvasLayer = $HUD
onready var score_timer: Timer = $ScoreTimer
onready var mob_timer: Timer = $MobTimer
onready var start_timer: Timer = $StartTimer

export (PackedScene) var Mob

var _rng: = RandomNumberGenerator.new()
var _score: = 0 setget _set_score


func _ready() -> void:
	Events.connect("start_game", self, "_on_Events_start_game")
	Events.connect("player_hit", self, "_on_Events_player_hit")
	score_timer.connect("timeout", self, "_on_ScoreTimer_timeout")
	mob_timer.connect("timeout", self, "_on_MobTimer_timeout")
	start_timer.connect("timeout", self, "_on_StartTimer_timeout")
	_rng.randomize()


func _on_Events_start_game() -> void:
	self._score = 0
	player.start()
	start_timer.start()
	hud.show_message("Get Ready")


func _on_Events_player_hit() -> void:
	score_timer.stop()
	mob_timer.stop()
	hud.show_game_over()


func _on_ScoreTimer_timeout() -> void:
	self._score += 1


func _on_MobTimer_timeout() -> void:
	mob_spawner.unit_offset = _rng.randf()
	var mob: RigidBody2D = Mob.instance()
	add_child(mob)
	mob.position = mob_spawner.position
	mob.rotation = calc_mob_rotation(_rng.randf_range(-PI/4, PI/4), mob_spawner.rotation)
	mob.linear_velocity = calc_mob_linear_velocity(_rng.randf_range(mob.min_speed, mob.max_speed), mob.rotation)


func _on_StartTimer_timeout() -> void:
	mob_timer.start()
	score_timer.start()


func _set_score(val: int) -> void:
	_score = val
	hud.score_label.text = str(_score)


static func calc_mob_rotation(offset: float, rotation: float) -> float:
	var out: = rotation + PI/2
	out += offset
	return out


static func calc_mob_linear_velocity(speed: float, rotation: float) -> Vector2:
	return Vector2(speed, 0).rotated(rotation)
