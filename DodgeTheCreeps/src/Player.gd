extends Area2D


onready var collision_shape: CollisionShape2D = $CollisionShape2D
onready var animated_sprite: AnimatedSprite = $AnimatedSprite

export var speed: = 400

var _start_position: = Vector2.ZERO
var _screen_size: = Vector2.ZERO


func _ready() -> void:
	connect("body_entered", self, "_on_Player_body_entered")
	_start_position = position
	_screen_size = $"/root".size


func _on_Player_body_entered(body: PhysicsBody2D):
	hide()
	Events.emit_signal("player_hit")
	collision_shape.set_deferred("disabled", true)


func _process(delta: float) -> void:
	var velocity: = _get_direction() * speed
	position = _calc_position(position, velocity, delta, _screen_size)
	_update_animation(velocity)


func start():
	position = _start_position
	collision_shape.disabled = false
	show()


func _update_animation(velocity: Vector2) -> void:
	if velocity.length() > 0:
		animated_sprite.play()
	else:
		animated_sprite.stop()

	if velocity.x != 0:
		animated_sprite.animation = "right"
		animated_sprite.flip_v = false
		animated_sprite.flip_h = velocity.x < 0
	elif velocity.y != 0:
		animated_sprite.animation = "up"
		animated_sprite.flip_v = velocity.y > 0


static func _calc_position(position: Vector2, velocity: Vector2, delta: float, screen_size: Vector2) -> Vector2:
	var out: = position + velocity * delta
	out.x = clamp(out.x, 0, screen_size.x)
	out.y = clamp(out.y, 0, screen_size.y)
	return out


static func _get_direction() -> Vector2:
	var out: = Vector2.ZERO
	out.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	out.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	return out.normalized()


