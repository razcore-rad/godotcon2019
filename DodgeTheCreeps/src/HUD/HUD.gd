extends CanvasLayer


onready var scene_tree: SceneTree = get_tree()
onready var start_button: Button = $StartButton
onready var score_label: Label = $ScoreLabel
onready var message_label: Label = $MessageLabel


func _ready() -> void:
	start_button.connect("pressed", self, "_on_StartButton_pressed")


func _on_StartButton_pressed():
	Events.emit_signal("start_game")
	start_button.hide()
	yield(scene_tree.create_timer(1), "timeout")
	message_label.hide()


func show_message(text):
	message_label.text = text
	message_label.show()


func show_game_over():
	show_message("Game Over")
	yield(scene_tree.create_timer(1), "timeout")
	message_label.text = "Dodge the\nCreeps"
	message_label.show()
	yield(scene_tree.create_timer(1), 'timeout')
	start_button.show()
